# BOT

TODO: 
- make app function the same way js version does
- implement processes & clean coding
- implement macro's


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `shouldibot` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:shouldibot, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/shouldibot](https://hexdocs.pm/shouldibot).

